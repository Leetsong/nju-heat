import Vue from 'vue';
import Router from 'vue-router';
import CONST from '../assets/const';
import { createPage } from '../pages/createpage';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/' + CONST.TYPE.NJU
    },
    {
      path: '/' + CONST.TYPE.NJU,
      redirect: '/' + CONST.TYPE.NJU + '/1'
    },
    {
      path: '/' + CONST.TYPE.JW,
      redirect: '/' + CONST.TYPE.JW + '/1'
    },
    {
      path: '/' + CONST.TYPE.CS,
      redirect: '/' + CONST.TYPE.CS + '/1'
    },
    {
      path: '/' + CONST.TYPE.NJU + '/:page',
      name: CONST.TYPE.NJU,
      component: createPage(CONST.TYPE.NJU)
    },
    {
    	path: '/' + CONST.TYPE.JW + '/:page',
    	name: CONST.TYPE.JW,
    	component: createPage(CONST.TYPE.JW)
    },
    {
      path: '/' + CONST.TYPE.CS + '/:page',
      name: CONST.TYPE.CS,
      component: createPage(CONST.TYPE.CS)
    }
  ]
});

// export default new Router({
//   routes: [
//     {
//       path: '/',
//       redirect: '/' + CONST.TYPE.NJU
//     },
//     {
//       path: '/' + CONST.TYPE.NJU,
//       name: CONST.TYPE.NJU,
//       component: createPage(CONST.TYPE.NJU)
//     },
//     {
//       path: '/' + CONST.TYPE.JW,
//       name: CONST.TYPE.JW,
//       component: createPage(CONST.TYPE.JW)
//     },
//     {
//       path: '/' + CONST.TYPE.CS,
//       name: CONST.TYPE.CS,
//       component: createPage(CONST.TYPE.CS)
//     }
//   ]
// });