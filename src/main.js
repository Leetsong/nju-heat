// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';

// inject router and store to the root app,
// to enable this.$router and this.$store
var app = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
