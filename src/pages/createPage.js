import CONST from '../assets/const';
import Page from './Page.vue';

export function createPage(name) {
	return {
		name: name,
		// template: `
		// 	<page :key="pageKey" :pageName="pageName"></page>
		// `,
		template: `
			<page :pageName="pageName"></page>
		`,
		data: function() {
			return {
				pageName: name
			};
		},
		computed: {
			pageKey: function() {
				return name + this.$route.params.page;
			}
		},
		components: {
			Page
		},
		beforeMount: function() {
			this.$store.dispatch(
				CONST.STORE.DISPATCH.FETCH_ITEMS, 
				{ 
					type: name, 
					fromPage: parseInt(this.$route.params.page)
				}
			);
		}
	};
}