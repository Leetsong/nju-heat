import Vue from 'vue';
import Vuex from 'vuex';
import api from './api';
import CONST from '../assets/const'

Vue.use(Vuex);

var store = new Vuex.Store({
	state: {
		activeType: "_padding",
		items: {
			'_padding': [],
			[CONST.TYPE.NJU]: [],
			[CONST.TYPE.JW]: [],
			[CONST.TYPE.CS]: []
		},
		maxPages: {
			'_padding': 0,
			[CONST.TYPE.NJU]: 0,
			[CONST.TYPE.JW]: 0,
			[CONST.TYPE.CS]: 0
		},
		MAX_ITEMS_PER_PAGE: 20
	},
	
	getters: {
		activeItems: function(state) {
			return state.items[state.activeType];
		},
		activeMaxPages: function(state) {
			return state.maxPages[state.activeType];
		},
		maxItemsPerPage: function(state) {
			return state.MAX_ITEMS_PER_PAGE;
		}
	},

	mutations: {
		[CONST.STORE.COMMIT.SET_ACTIVE_TYPE]: function(state, { type }) {
			state.activeType = type;
		},
		[CONST.STORE.COMMIT.SET_ITEMS]: function(state, { type, items }) {
			// vue.set ?
			state.items[type] = items;
		},
		[CONST.STORE.COMMIT.SET_MAX_PAGES]: function(state, { type, mP }) {
			state.maxPages[type] = mP;
		},
		[CONST.STORE.COMMIT.INCREASE_HEAT]: function(state, { type, id }) {
			state.items[type].forEach(function(item) {
				if(item.id === id) {
					item.heat += 1;
				}
			})
		}
	},

	actions: {
		[CONST.STORE.DISPATCH.FETCH_ITEMS]: function({ state, commit }, { type, fromPage }) {
			commit('SET_ACTIVE_TYPE', { type });
			if(state.maxPages[type] === 0) {
				api.fetchItemCount(type)
				.then(function(mI) {
					commit('SET_MAX_PAGES', { type, mP: Math.ceil(mI / state.MAX_ITEMS_PER_PAGE) });
				});
			}

			api.fetchItems(type, (fromPage - 1) * state.MAX_ITEMS_PER_PAGE, state.MAX_ITEMS_PER_PAGE)
			.then(function(items) {
				commit(CONST.STORE.COMMIT.SET_ITEMS, { type, items });
			}).catch(function(err) {
				console.log(err);
			});
		},
		[CONST.STORE.DISPATCH.INCREASE_HEAT]: function({ state, commit }, { type, id }) {
			api.increaseHeat(type, id)
			.then(function(result) {
				if(result.succ) {
					commit(CONST.STORE.COMMIT.INCREASE_HEAT, { type, id });
				}
			}).catch(function(err) {
				console.log(err);
			});
		},
		[CONST.STORE.DISPATCH.POST_ISSUE]: function({ state, commit }, { issue }) {
			return api.postIssue(issue)
				.then(function(result) {
					return result.succ;
				});
		}
	}
});

export default store;