import Vue from 'vue';
import VueResource from 'vue-resource';
import Cache from 'lrucache';
import CONST from '../assets/const'

Vue.use(VueResource);

// client-layer cache

var cache = Cache({
  max: 1000,
  maxAge: 15 * 60 * 1000 // 15 min cache
});

function convertToCacheKey({ type, fromIndex }) {
	return `${type}:${fromIndex}`;
}

function fetchItemCount(type) {
	return Vue.http.get(`/?method=${CONST.AJAX.GET.FETCH_ITEM_COUNT}&type=${type}&itemCount=true`)
		.then(function(response) {
				// response.json() conveys a json obj and return a promise
				return response.json();
		}, function(err, response) {
			return new Promise(function(resolve, reject) {
				reject(response.status + ": " + response.statusText);
			});
		}); 
}

function fetchItems(type, fromIndex, count) {
	if(cache.has(convertToCacheKey({ type, fromIndex }))) {
		return new Promise(function(resolve, reject) {
			resolve(cache.get(convertToCacheKey({ type, fromIndex })));
		});
	}

	return Vue.http.get(`/?method=${CONST.AJAX.GET.FETCH_ITEMS}&type=${type}&fromIndex=${fromIndex}&count=${count}`)
		.then(function(response) {
				// response.json() conveys a json obj and return a promise
				return response.json().then(function(json) {
					cache.set(convertToCacheKey({ type, fromIndex }), json);
					return json;
				});
		}, function(err, response) {
			return new Promise(function(resolve, reject) {
				reject(response.status + ": " + response.statusText);
			});
		});
}

function increaseHeat(type, id) {
	var body = {
		type,
		id
	};
	return Vue.http.post(`/?method=${CONST.AJAX.POST.INCREASE_HEAT}`, body)
	.then(function(response) {
		return response.json();
	}, function(err, response) {
		return new Promise(function(resolve, reject) {
			reject(response.status + ": " + response.statusText);
		});
	});
}

function postIssue(issue) {
	var body = {
		issue
	};
	return Vue.http.post(`/?method=${CONST.AJAX.POST.POST_ISSUE}`, body)
		.then(function(response) {
			return response.json();
		}, function(err) {
			return false;
		});
}

export default {
	fetchItems,
	fetchItemCount,
	increaseHeat,
	postIssue
};