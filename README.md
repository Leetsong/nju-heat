# nju-heat

> nju-heat: a simple web site collecting nju hot informations

<p align="center">
    <img src="static/demo.png">
</p>

## status

***under construction***

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm run dev-serv

# proxy-serve with hot reload at localhost:8080
npm run dev-cli

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Reuquirements

1. ~~Extract all raw strings to a seperate file~~
2. ~~vue-resource~~
3. ~~LRU cache, when heat updates, cache is dirty; lrucache only supports string as key~~
4. ~~Multiple HTTP(which means that, we will never acquire all the infomation in one http, but by blocks)~~
5. ~~Make every partial list of one large page separated, which can be implemented by dynamic route~~
6. Seperate client and server
7. loading and other ~~transition~~ or animation effects
8. change store.state.items to Map to increate query time(when increase heat)
9. think over: can store.state.item use dirty bit?
10. more pages, bbs/ten bbs/jw bbs/headmaster bbs/cs
11. handle unexpected URL
12. add LRU cache in server end
13. crawler system, make it more useable and helpful for crawler, make crawler more concentrated and care little about when to start, when to end
14. handle err of serv&cli
15. when crawler crawls something new, it has to inform the front-end: `websocket`?
16. ~~when crawler crawls something new, the item.id will be wrong: use sql `LIMIT` to modify it, suitable for small data~~, and will generate duplicate in db.
17. add a script to control server start-up and termination, and add a log file to save the `stdout` and `stderr`
18. change all chinese to english. T_T
19. ~~add a window to serve as an assistant, allowing users to give suggestions,~~ in which case, server has to have a mechanism that can signal the back-end(this mechanism can be implemented by a back-end management website).
20. add machine learning algorithms to filter classify issues.

## Crawler System

We add a crawler system to server, this system crawls the needed pages every 1 hour by default, and we can set it to other interval by `Crawler.prototype.setInterval`.

We can easily insert a crawler into it when we strictly follow the following requirements:

1. The name of the main file of any crawler has to match `/(-crawler.js)$/`, and place it under `/server/crawler/`
2. Every crawler has to expose a method named `performOnce`.
3. `performOnce` method performs one pass of a crawling.
4. `performOnce` takes a parameter of type `PoolConnection` of `mysql`. Via this parameter, crawler can communicate with the database.

for more information of crawler, refer to `/server/crawler/`