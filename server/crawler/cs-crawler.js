var url = require('url');
var req = require('superagent');
var cheerio = require('cheerio');
var EventProxy = require('eventproxy');

var base = 'http://cs.nju.edu.cn';

var lastRecord = undefined;

function performOnce(connection) {
	console.log("[" + new Date().toUTCString() + "] ready to crawl " + base);

	getMaxRecord()
	.then(getMaxPage)
	.then(performCrawl);

	function performCrawl(maxPage) {
		var container = [];
		var urlPool = [];
		var ep = new EventProxy();

		for(var i = 1; i <= maxPage; i ++) {
			urlPool.push(`${base}/1654/list${i}.htm`);
		}

		ep.after('page done', urlPool.length, tailHandling);

		while(urlPool.length) {
			var thisPageUrl = urlPool.shift();
			console.log('Crawl: ' + thisPageUrl);
			req.get(thisPageUrl).end(function(err, res) {
				if(!err) {
					var $ = cheerio.load(res.text);
					$('#wp_news_w82').find('li').each(function(index, el) {
						var divs = $(el).find('div');
						container.push({
							href: base + divs.eq(0).find('a').eq(0).attr('href'),
							title: divs.eq(0).find('a').eq(0).text(),
							time: Date.parse(divs.eq(1).find('span').eq(0).text()),
							heat: 0
						});
					});
					ep.emit('page done');
				}
			});
		}
		
		function tailHandling(list) {
			var count = 0;

			ep.after('query done', container.length, function() {
				lastRecord += count;
				connection.release();
				console.log("[" + new Date().toUTCString() + "] crawl done: " + count + "/" + lastRecord);
			});

			container.forEach(function(item, index) {
				connection.query({
					sql: '\
						INSERT INTO cs \
							(title, href, time, heat) \
						VALUES \
							(?, ?, ?, ?)\
					',
					values: [item.title, item.href, item.time, item.heat]
				}, function(err) {
					if(err) {
						console.log(err);
					} else {
						count ++;
					}
					ep.emit('query done');
				});
				// console.log("%d: [%d] %s\n  %s", count++, item.time, item.title, item.href);
				// ep.emit('query done');
			});
		}
	}

	function getMaxPage(maxRecord) {
		return new Promise(function(resolve, reject) {
			// 刚启动，获取数据库信息数目
			if(!lastRecord) {
				connection.query('SELECT COUNT(id) FROM cs', function(err, results, fields) {
					if(err) {
						console.error(err);
						return ;
					}
					lastRecord = results[0]["COUNT(id)"];
					// 与上次信息数目相同，不需要爬取
					if(lastRecord === maxRecord) {
						console.log("[" + new Date().toUTCString() + "] Database is newest!");
						reject("Database is newest!");
					} else {
						// 爬取
						req.get(base + '/1654/list1.htm').end(function(err, res) {
							if(err) {
								reject(err);
							} else {
								var $ = cheerio.load(res.text);
								var maxPage = $('#wp_paging_w82').find('em.all_pages').eq(0).text();
								resolve(parseInt(maxPage));	
							}
						});
					}
				});
			} else if(lastRecord === maxRecord) {
				// 与上次信息数目相同，不需要爬取
				console.log("[" + new Date().toUTCString() + "] Database is newest!");
				reject("Database is newest!");
			} else {
				// 爬取
				req.get(base + '/1654/list1.htm').end(function(err, res) {
					if(err) {
						reject(err);
					} else {
						var $ = cheerio.load(res.text);
						var maxPage = $('#wp_paging_w82').find('em.all_pages').eq(0).text();
						resolve(parseInt(maxPage));	
					}
				});
			}
		});
	}

	function getMaxRecord() {
		return new Promise(function(resolve, reject) {
			req.get(base + '/1654/list1.htm').end(function(err, res) {
				if(err) {
					reject(err);
				} else {
					var $ = cheerio.load(res.text);
					var maxRecord = $('#wp_paging_w82').find('em.all_count').eq(0).text();
					resolve(parseInt(maxRecord));
				}
			})
		})
	}
}

var csCrawler = {
	performOnce: performOnce
};

module.exports = csCrawler;