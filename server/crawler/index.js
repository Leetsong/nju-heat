var fs = require('fs');
var dbpool = require('../db').pool;

function Crawler(interval) {
	this._interval = interval || 1 * 3600 * 1000; // 1h interval by default
	this._list = [];
	this._timer = undefined;
	this._initiated = false;
};

Crawler.prototype.setInterval = function(interval) {
	this._interval = interval;
	if(this._timer) {
		clearInterval(this._timer);
		this.run();
	}
};

Crawler.prototype.run = function(callback) {
	if(this._timer) return;
	if(!this._initiated) this._init();
	if(callback) callback();
	setTimeout(this._performOnce, 0, this);
	this._timer = setInterval(this._performOnce, this._interval, this);
};

Crawler.prototype.shutdown = function(callback) {
	if(this._timer) {
		clearInterval(this._timer);
		callback();
	}
}

Crawler.prototype._init = function() {
  var files = fs.readdirSync(__dirname);
  var crawler = files.filter(function(f) {
      return f.match(/(-crawler\.js)$/);
  });
  var that = this;
  crawler.forEach(function(f) {
  	var c = require('./' + f.slice(0, -3));
  	if("performOnce" in c) {
			that._list.push(c);
		} else {
			throw new TypeError("a crawler must have a method called `performOnce`");
		}
  });
  this._initiated = true;
}

Crawler.prototype._performOnce = function(that) {
	var errC = [];	// we give those crawlers who failed to get connection another chance to get a new connection
	
	for(var i = 0, l = that._list.length; i < l; i ++) {
		let c = that._list[i];
		dbpool.getConnection(function(err, connection) {
			if(err) {
				errC.push(c);
			} else {
				c.performOnce(connection);
			}
		});
	}

	errC.forEach(function(c) {
		dbpool.getConnection(function(err, connection) {
			if(err) {
				throw err;
			} else {
				c.performOnce(connection);
			}
		});	
	});
}

var crawler = new Crawler();

module.exports = crawler;