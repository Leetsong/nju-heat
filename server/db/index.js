var mysql = require('mysql');
var pool = mysql.createPool(require('./config'));

function queryItemCount({ type }, callback) {

	pool.getConnection(function(err, connection) {
		if(err) {
			callback(err);
			return ;
		}

		var sql = `
			SELECT COUNT(id) FROM ${type}
		`;

		connection.query(sql, function(err, results) {
			connection.release();

			if(err) {
				callback(err);
				return ;
			}

			callback(err, results[0]["COUNT(id)"]);
		});
	});

}

function queryItems({ type, fromIndex, count }, callback) {

	pool.getConnection(function(err, connection) {
		if(err) {
			callback(err);
			return ;
		}

		// 搜索排序在 fromIndex ~ fromIndex+count-1 的
		var sql = `
			SELECT * 
			FROM ${type} 
			ORDER BY time DESC
			LIMIT ${fromIndex}, ${count}
		`;

		connection.query(sql, function(err, results) {
			connection.release();

			if(err) {
				callback(err);
				return ;
			}

			results.forEach(function(item) {
				item.type = type;
				item.time = new Date(item.time).toLocaleDateString().slice(0, 11); // YYYY-mm-dd
			})

			callback(err, results);
		});
	});

}

function increaseHeat({ type, id }, callback) {

	pool.getConnection(function(err, connection) {
		if(err) {
			callback(err);
			return;
		}

		var sql = `
			UPDATE ${type}
			SET heat = heat + 1
			WHERE id = ${id}
		`;

		connection.query(sql, callback);
	})

}

function postIssue({ issue }, callback) {

	pool.getConnection(function(err, connection) {
		if(err) {
			callback(err);
			return;
		}

		connection.query(
			{
				sql: `
					INSERT INTO issues
						(issue, open)
					VALUES
						(?, ?)
				`,
				values: [ issue, 'T' ]
			}, 
			callback
		);
	});

}

function shutdown(callback) {
	pool.end(callback);
}

module.exports = {
	pool: pool,
	queryItemCount: queryItemCount,
	queryItems: queryItems,
	increaseHeat: increaseHeat,
	postIssue: postIssue,
	shutdown: shutdown
};