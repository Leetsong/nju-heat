var router = require('express').Router();
var db = require('../db');

router.get('/', function(req, res) {
	switch(req.query.method) {
		case "FETCH_ITEMS": {
			db.queryItems(
				{ 
					type: req.query.type, 
					fromIndex: req.query.fromIndex, 
					count: req.query.count 
				}, 
				function(err, result) {
					if(err) {
						res.json([]);
					} else {
						res.json(result);
					}
				}
			);
			break;
		}
		case "FETCH_ITEM_COUNT": {
			db.queryItemCount(
				{ 
					type: req.query.type 
				}, 
				function(err, result) {
					if(err) {
						res.json([]);
					} else {
						res.json(result);
					}
				}
			);			
			break;
		}
	}
});

router.post('/', function(req, res) {
	switch(req.query.method) {
		case 'INCREASE_HEAT': {
			db.increaseHeat({ type: req.body.type, id: req.body.id }, function(err) {
				if(err) {
					res.json({ succ: false });
				} else {
					res.json({ succ: true });
				}
			});	
			break;
		}
		case 'POST_ISSUE': {
			db.postIssue({ issue: req.body.issue }, function(err) {
				if(err) {
					res.json({ succ: false });
				} else {
					res.json({ succ: true });
				}
	 		});
			break;
		}
	}
});

module.exports = router;