var express = require('express');
var bodyParser = require('body-parser');
var db = require('./db');
var router = require('./router');
var crawler = require('./crawler');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

app.use('/', router);

var server = app.listen(3000, function() {
	console.log('START....');
	crawler.run(function() {
		console.log('  SUCCESS!\n');
	});
});

// 监听退出事件，监听后会改变 node 的默认行为，因此需要手动退出
process.on('SIGINT', shutdown); // ctrl-c

function shutdown() {
	console.log('STOP...');

	var task1 = new Promise(function(resolve) {
		crawler.shutdown(function() {
			resolve();
		});
	});

	var task2 = new Promise(function(resolve) {
		db.shutdown(function() {
			resolve();
		});
	});

	Promise.all([task1, task2]).then(function() {
		console.log(' SUCCESS!\n');
		process.exit(0);
	});
}